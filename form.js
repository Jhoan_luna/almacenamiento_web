
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCna-ivib9QpHul0aSwEqdpWz4ZQJ_WE7A",
  authDomain: "usuarios-f0f76.firebaseapp.com",
  projectId: "usuarios-f0f76",
  storageBucket: "usuarios-f0f76.appspot.com",
  messagingSenderId: "546133957586",
  appId: "1:546133957586:web:cdd292e4d45049fcc2860b",
  measurementId: "G-FW07BGRTMX"
};
  // Inicializacion de fire base Firebase
  firebase.initializeApp(firebaseConfig);

  const auth =  firebase.auth();

  //Registro function
  function signUp(){
    var email = document.getElementById("email");
    var password = document.getElementById("password");

    const promise = auth.createUserWithEmailAndPassword(email.value,password.value);
    //
    promise.catch(e=>alert(e.message));
    alert("Registro Exitoso!");
  }

  //Logueo function
  function  signIn(){
    var email = document.getElementById("email");
    var password  = document.getElementById("password");
    const promise = auth.signInWithEmailAndPassword(email.value,password.value);
    promise.catch(e=>alert(e.message));
    
  }


  //signOut

  function signOut(){
    auth.signOut();
    alert("Sesion cerrada del sistema");
  }

  //active user to homepage
  firebase.auth().onAuthStateChanged((user)=>{
    if(user){
      var email = user.email;
      alert("Usuario activo "+email);

    }else{
      alert("Ningun usuario encontrado activo")
    }
  })